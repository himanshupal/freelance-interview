export enum Platform {
  INSTAGRAM = "INSTAGRAM",
  TWITCH = "TWITCH",
  YOUTUBE = "YOUTUBE",
  OTHER = "OTHER"
}
