import { Platform } from "./platform.model"

export type QueryParams = {
  limit: number
  platforms?: Array<Platform>
  keywords?: Array<string>
  budgetGreaterEqual?: number
  budgetLowerEqual?: number
  skip?: number
}
