import { Platform } from "./platform.model"

type Budget = {
  value: number
  currecy: string
}

type Task = {
  id: string
  title: string
  description: string
  budget: Budget
  proposalCount: number
  platforms: Array<Platform>
}

export interface TasksModel {
  all: Array<Task>
}
