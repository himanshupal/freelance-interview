import { Platform } from "./platform.model"

export interface FilterModel {
  minBudget: number
  maxBudget: number
  selectedPlatforms: Array<Platform>
}
