import { ref } from "vue"

const DEFAULT_PAGE_NUMBER = 1

const pageNumber = ref(DEFAULT_PAGE_NUMBER)

const reset = () => (pageNumber.value = DEFAULT_PAGE_NUMBER)

const setPage = (page: number) => (pageNumber.value = page)

export default function usePagination() {
  return {
    pageNumber,

    setPage,
    reset
  }
}
