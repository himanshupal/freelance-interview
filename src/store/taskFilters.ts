import { ref, reactive } from "vue"
import { FilterModel } from "@/models/filters.model"

const searchTerms = ref<string>("")

const filters = reactive<FilterModel>({
  minBudget: 0,
  maxBudget: 0,
  selectedPlatforms: []
})

export default function useTaskFilters() {
  return {
    filters,
    searchTerms
  }
}
