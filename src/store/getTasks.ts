import { reactive, computed } from "vue"

import api, { paramsToQueryString } from "@/services/api"

import { QueryParams } from "@/models/api.model"
import { TasksModel } from "@/models/tasks.model"

import useTaskFilters from "./taskFilters"
import usePagination from "./paginate"

export const TASKS_PER_PAGE = 3

const { filters, searchTerms } = useTaskFilters()
const { pageNumber, reset: pageReset } = usePagination()

const tasks = reactive<TasksModel>({
  all: []
})

const getTasks = async (
  reset = true,
  ...restParams: Array<Record<string, string | number>>
) => {
  if (reset) pageReset()

  let queryParams: QueryParams = {
    limit: reset ? TASKS_PER_PAGE * 2 : TASKS_PER_PAGE,
    skip: reset ? 0 : TASKS_PER_PAGE * (pageNumber.value - 1) + TASKS_PER_PAGE
  }

  if (searchTerms.value) {
    queryParams = {
      ...queryParams,
      keywords: searchTerms.value.split(" ")
    }
  }

  if (filters.maxBudget) {
    queryParams = {
      ...queryParams,
      budgetLowerEqual: Number(filters.maxBudget)
    }
  }

  if (filters.minBudget) {
    queryParams = {
      ...queryParams,
      budgetGreaterEqual: Number(filters.minBudget)
    }
  }

  if (filters.selectedPlatforms.length) {
    queryParams = {
      ...queryParams,
      platforms: filters.selectedPlatforms
    }
  }

  restParams.forEach(parameter => {
    queryParams = { ...queryParams, ...parameter }
  })

  const params = paramsToQueryString(queryParams)

  const { data } = await api.get(`/tasks${params}`)

  tasks.all = reset ? data.tasks : [...tasks.all, ...data.tasks]
}

const taskList = computed(() => {
  return tasks.all.slice(
    TASKS_PER_PAGE * (pageNumber.value - 1),
    TASKS_PER_PAGE * pageNumber.value
  )
})

export default function useTasks() {
  return {
    tasks,
    taskList,
    getTasks
  }
}
